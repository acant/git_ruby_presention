# Motivation

* version all the things!
* learn enough of the rugged interface to build git_simple

# Psst....

* everything is secretly a wiki

# Psst....

* everything is secretly a git repository

# DAG?

# Also want to use Ruby

# [gitdocs](https://github.com/nesquena/gitdocs)

# [ppl](https://github.com/hnrysmth/ppl)

# Git's data structure

https://git-scm.com/book/en/v2/Git-Internals-Git-Objects
http://gitready.com/beginner/2009/02/17/how-git-stores-your-data.html
https://en.wikibooks.org/wiki/Git/Internal_structure
https://git-scm.com/docs/gitglossary

# libgit2 gives us A LOT to work with

https://libgit2.github.com/libgit2/#HEAD

# Goal

* eliminate dependency on git CLI
* eventually get better control of the SSH connections

# CLI commands to implement

* git init
* git log
* git commit
* git pull
* git push

# Notes about the code

* trying to use Pathname
* cover simple cases that I need
  - view details of the repo
  - push and pull
  - create straight forward commands
* add more in the future

# Credentials for HTTP

```
credentials =
  Rugged::Credentials::UserPassword.new(
    username: 'user',
    password: 'password',
  )
```

# Credentials for SSH

```
credentials =
  Rugged::Credentials::SshKey.new(
    username:   'git',
    publickey:  Pathname(ENV['HOME']).join('.ssh/id_rsa.pub').to_s,
    privatekey: Pathname(ENV['HOME']).join('.ssh/id_rsa').to_s,
    # passphrase: ENV["GITTEST_REMOTE_SSH_PASSPHASE"]
  )
```

# Credentials from the SSH agent

```
credentials =
  Rugged::Credentials::SshKeyFromAgent.new(
    username: 'git'
  )
```

# Clone

```
pathname = Pathname('test_repo')
ssh_url  = 'git@gitlab.com:acant/test_repo.git'

pathname.mkpath
Rugged::Repository.clone_at(ssh_url, pathname.to_s, credentials: credentials)
```

# Add files to the index

```
pathname = Pathname('test_repo')
now = DateTime.now
new_filename = pathname.join("#{now.strftime('file%H%M%S')}.txt")
new_filename.write(now.to_s)

rugged = Rugged::Repository.discover(new_filename.to_s)

relative_new_filename = new_filename.realpath.relative_path_from(Pathname(rugged.workdir))
rugged.index.add(relative_new_filename.to_s)
rugged.index.write
```

# Create a new commit

```
author = {
  name:  rugged.config['user.name'],
  email: rugged.config['user.email'],
  time:  Time.now
}
Rugged::Commit.create(
  rugged,
  tree:       rugged.index.write_tree(rugged),
  author:     author,
  committer:  author,
  message:    "Commit at #{now.to_s}",
  parents:    [rugged.head.target].compact,
  update_ref: 'HEAD'
)
```

# Push

```
rugged = Rugged::Repository.discover(pathname.to_s)
rugged.remotes['origin'].push(%w[refs/heads/master], credentials: credentials)
```

# Pull, fast-forward

```
rugged.fetch('origin', credentials: credentials)
# Possible response :normal, :up_to_date, :fastforward, :unborn
rugged.merge_analysis('origin/master')

rugged.references.update('refs/heads/master', rugged.branches['origin/master'].target_id)
rugged.checkout_head(strategy: :force)
```

# Pull, merge commit

```
ours   = rugged.rev_parse('master')
theirs = rugged.rev_parse('origin/master')
base   = rugged.rev_parse(rugged.merge_base(ours, theirs))
index = ours.tree.merge(theirs.tree, base.tree)

author = ...
Rugged::Commit.create(
  rugged,
  tree:       index.write_tree(rugged), # This does require an explicit repository
  author:     author,
  committer:  author,
  message:    "Merge branch 'master' of gitlab.com:acant/test_repo",
  parents:    [ours, theirs], #[rugged.head.target].compact,
  update_ref: 'HEAD'
)
rugged.checkout_head(strategy: :force)
```

# Log and Status

```
rugged.references.each { |x| pp x }

rugged.remotes.each { |x| pp x }

rugged.branches.each { |x| pp x }

walker = Rugged::Walker.new(rugged)
walker.sorting(Rugged::SORT_DATE)
walker.push(rugged.head.target)
walker.each { |x| pp x }
```

# Outline of the git_simple API

```
GitSimple('repo_path')
  .add('new_file')
  .rm('old_file')
  .commit('Made some changes', name: 'Art T. Fish', email: 'afish@example.com')
```

# Have a nice day

* https://gitlab.com/acant/git_ruby_presention

* https://github.com/acant/git_simple
